import { callApi } from '../helpers/apiHelper';


function getRandomInteger(min, max) {
  let rand = min + Math.random() * (max + 1 - min);
  return rand;
}

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export class Fighter {
  constructor(name, health, attack, defense) {
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
    this.criticalHitChance = getRandomInteger(1,2);
    this.dodgeChance = getRandomInteger(1,2);
  }

  getHitPower() {
    const power = this.attack * this.criticalHitChance;
    return power;
  }

  getBlockPower() {
    const power = this.defense * this.dodgeChance;
    return power;
  }

}


export const fighterService = new FighterService();

