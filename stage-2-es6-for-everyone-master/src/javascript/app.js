import { createFighters } from './components/fightersView';
import { fighterService, Fighter } from './services/fightersService';

const randomId = 1;
const fighterDetails = fighterService.getFighterDetails(randomId)
  .then(({attack, defense, name, health}) => {
    const fighter = new Fighter(name, health, attack, defense);
    console.log('Fighter attack power', fighter.getHitPower());
    console.log('Fighter block power', fighter.getBlockPower());
  });

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
