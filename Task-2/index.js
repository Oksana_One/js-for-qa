//1.Pop The Bubble game. 
// Я закоментувала код, його запускаю лише в консолі гри
/*
setTimeout(function() {
  const bubbles = document.querySelectorAll('.bubble');
console.log(bubbles.length);
  for (let i of bubbles) {
      i.click();
  }
const score = document.querySelector('#score');
console.log(bubbles.length === Number(score.textContent));
}, 5000);

//Бонус
function countAndClick() {
  const bubbles = document.querySelectorAll('.bubble');
  if (bubbles.length) {
    for (let i of bubbles) {
      i.click();
  }
  const score = document.querySelector('#score');
  console.log(Number(score.textContent));
  setTimeout(countAndClick, 350);
 } else {
      setTimeout(countAndClick, 350);
  }
} 
countAndClick();
*/

//2.Написать функцию superSort(value)...
function superSort(line) {
  const array = line.split(' ');
  const sortedArray = array.sort(function(a, b) {
    if (a.replace(/[0-9]/g, '') > b.replace(/[0-9]/g, '')) {
      return 1;
    }
    else {
      return -1;
    }
  });
  return sortedArray.join(' ');
}

const line  = "mic09ha1el 4b5en6 michelle be4atr3ice";
const sortedArray = superSort(line);
console.log(sortedArray);

// 3.Написать функцию, которая будет сравнивать два значения...
function isEqualDate(date1, date2) {
  const regExp = /(\/| |-|\.|\,)/g;

  const firstDate = date1.split(regExp);
  const secondDate = date2.split(regExp);

  return firstDate[0] === secondDate[0]
      && firstDate[2] === secondDate[2]
      && firstDate[4] === secondDate[4];

}

date1 = "25.02.2005";
date2 = "25.02.2005";
console.log(isEqualDate(date1, date2));

//4.Есть массив с множеством чисел. Все числа одинаковы, кроме одного. Попробуйте найти это число!
function find(arr) {
  const sorted = arr.sort();
  if (sorted[0] !== sorted[1]) {
    return sorted[0];
  }
  return sorted[sorted.length -1];
}

  const arr = [1, 1, 1, 2, 1, 1];
  console.log(find(arr));

//5.Используя if..else или условный (тернарный) оператор завершите функцию getDiscount
function getDiscount (number) {
  let discount = 0.0;
  if (number >= 5 && number < 10) {
    discount = 0.05;
  }
  else if (number >= 10.0) {
    discount = 0.1;
  }
  return (1.0-discount)*number;
}

const number = 7;
console.log(getDiscount (number));

//6.Используя switch statement напишите функцию getDaysForMonth...
function getDaysForMonth(month) {
  if (month <= 0 || month > 13) {
    throw new Error("Month must be between 1 and 12");
  } 
  else {
    switch(Number(month)) {
      case 1: 
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        return 31;
      case 4:
      case 6:
      case 9:
      case 11:
        return 30;
      case 2: 
        return 28;
      default:
        return;
    }
  } 
}

const month = 1;
console.log(getDaysForMonth(month));